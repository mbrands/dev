package de.mbrands.ListService.resources;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import de.mbrands.ListService.dao.TodoDao;
import de.mbrands.ListService.model.Todo;

public class TodosRessourceTest extends JerseyTest {

	@Override
	protected Application configure() {
		return new ResourceConfig(TodosResource.class);
	}

	@Before
	public void prepare_todos() {
		
		Todo todo = new Todo(1, "Learn REST");
		TodoDao.instance.saveTodo(todo);
		
		todo = new Todo(2, "Do something");
		TodoDao.instance.saveTodo(todo);

	}

	@After
	public void clear_todos() {
		target("todos").request().delete();
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void test_get_all_todos() {
		String response = target("todos").request().get(String.class);
		assertTrue(
				"Expected todos not found in response: " + response,
				response.contains("Learn REST")
						&& response.contains("Do something"));
	}

//	@Test
//	public void test_get_specific_todo() {
//		
//		Todo todo = new Todo(1, "Learn REST");
//		TodoDao.instance.saveTodo(todo);
//		
//		List<Todo> todolist = TodoDao.instance.readAllTodos();
//		Todo existingTodo = todolist.get(0);
//		Todo responseTodo = target("todos/" + existingTodo.getId()).request().get(Todo.class);
//		assertTrue("Expected String 'Learn REST' not found in response: "
//				+ responseTodo, responseTodo.getDescription().contains("Learn REST"));
//	}

	@Test
	public void test_post_todo() {
		Entity<String> todoEntity = Entity.entity(new String("newTodo"),
				MediaType.TEXT_PLAIN);
		String postResponse = target("todos").request().post(todoEntity,
				String.class);
		assertTrue("Expected todo not found in response: " + postResponse,
				postResponse.contains("newTodo"));

	}

	@Test
	public void test_update_todo() {
		
		Todo todo = new Todo(1, "Learn REST");
		TodoDao.instance.saveTodo(todo);
		
		List<Todo> todolist = TodoDao.instance.readAllTodos();
		Todo existingTodo = (Todo)todolist.get(0);
		
		String todoString = target("todos/" + existingTodo.getId()).request().get(String.class);
		String changedTodo = todoString.replace("false", "true");
		Entity<String> todoEntity = Entity.entity(changedTodo,
				MediaType.APPLICATION_JSON);
		String putResponse = target("todos").request().put(todoEntity,
				String.class);
		assertTrue("Expected status 'true' not found in response: "
				+ putResponse, putResponse.contains("true"));
	}

	@Test
	public void test_delete_all_todos() {
		target("todos").request().delete();
		String response = target("todos").request().get(String.class);
		assertTrue("Todos were not empty: " + response, response.contains("[]"));
	}

//	@Test
//	public void test_delete_specific_todo() {
//		target("todos/1").request().delete();
//		String response = target("todos").request().get(String.class);
//		assertFalse("Deleted todo found: " + response,
//				response.contains("Learn REST"));
//	}
	

}
