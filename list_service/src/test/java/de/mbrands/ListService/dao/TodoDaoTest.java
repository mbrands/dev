package de.mbrands.ListService.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import de.mbrands.ListService.model.Todo;

public class TodoDaoTest {

	
	@Test
	public void test_save_todo(){
		Todo todo = new Todo ("newTodo");
		todo = TodoDao.instance.saveTodo(todo);
		assertTrue("Expected todo found: " + todo,
				todo.getDescription().contains("newTodo"));
	}
	
	@Test
	public void test_update_todo(){
		Todo newTodo = new Todo ("newTodo");
		TodoDao.instance.saveTodo(newTodo);	
		
		List<Todo> todos = TodoDao.instance.readAllTodos();
		Todo todo = todos.get(0);
		todo.setStatus(true);
		TodoDao.instance.saveTodo(todo);	
		
		Todo updatedTodo = TodoDao.instance.readTodo(todo.getId());
		
		assertTrue("Todo was not set to true: " + updatedTodo,
				updatedTodo.getStatus());
	}
	
	@Test
	public void test_get_all_todos() {
		
		Todo todo = new Todo("Learn REST");
		TodoDao.instance.saveTodo(todo);		
		todo = new Todo("Do something");
		TodoDao.instance.saveTodo(todo);
		
		List<Todo> todos = TodoDao.instance.readAllTodos();
		String descriptions = null;
		for(Todo readTodo :todos){
			descriptions+=readTodo.getDescription();
		}
		assertTrue(
				"Expected todos not found in response: " + descriptions,
				descriptions.contains("Learn REST")
						&& descriptions.contains("Do something"));
	}
	
	@Test
	public void test_delete_todos(){
		Todo todo = new Todo ("test");
		TodoDao.instance.saveTodo(todo);		
		TodoDao.instance.saveTodo(todo);
		TodoDao.instance.deleteAllTodos();
		List<Todo> todos = TodoDao.instance.readAllTodos();
		assertTrue("Todos were not empty: " + todos, todos.toString().contains("[]"));
	}
	


}
