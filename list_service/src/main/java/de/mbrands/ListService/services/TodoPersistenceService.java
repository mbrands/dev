package de.mbrands.ListService.services;

import java.util.List;

import de.mbrands.ListService.dao.TodoDao;
import de.mbrands.ListService.exception.NotFoundException;
import de.mbrands.ListService.model.Todo;

public class TodoPersistenceService {

	public Todo getSpecificTodo(long id) throws NotFoundException {

		Todo todo = TodoDao.instance.readTodo(id);
		if (todo == null)
			throw new NotFoundException("Get: Todo with " + id + " not found");
		return todo;
	}

	public List<Todo> getAllTodos() {
		return TodoDao.instance.readAllTodos();
	}

	public Todo addTodo(String description) {
		Todo todo = new Todo(description);
		return TodoDao.instance.saveTodo(todo);
	}

	public Todo updateTodo(Todo todo) throws NotFoundException {

		return TodoDao.instance.saveTodo(todo);

	}

	public void deleteSpecificTodo(long id) throws NotFoundException {
		TodoDao.instance.deleteTodo(id);
	}

	public void deleteAllTodos() {
		TodoDao.instance.deleteAllTodos();
	}

}
