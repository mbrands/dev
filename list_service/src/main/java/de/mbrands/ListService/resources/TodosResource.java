package de.mbrands.ListService.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import de.mbrands.ListService.exception.NotFoundException;
import de.mbrands.ListService.model.Todo;
import de.mbrands.ListService.services.TodoPersistenceService;

@Path("/todos")
public class TodosResource {

	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	TodoPersistenceService persistenceService = new TodoPersistenceService();

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Todo> getTodos() {
		return persistenceService.getAllTodos();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Todo getTodo(@PathParam("id") long id) throws NotFoundException {
		return persistenceService.getSpecificTodo(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	public Todo newTodo(String description) {

		return persistenceService.addTodo(description);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Todo updateTodo(Todo sentTodo) throws NotFoundException {

		return persistenceService.updateTodo(sentTodo);

	}

	@DELETE
	public void deleteTodos() {
		persistenceService.deleteAllTodos();
	}

	@DELETE
	@Path("{id}")
	public void deleteTodo(@PathParam("id") long id) throws NotFoundException {
		persistenceService.deleteSpecificTodo(id);
	}

}