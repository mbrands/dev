package de.mbrands.ListService.dao;

import java.util.List;

import javax.persistence.EntityManager;

import de.mbrands.ListService.model.Todo;

public enum TodoDao {
	instance;

	private EntityManager entityManager = EntityManagerUtil.getEntityManager();

	private TodoDao() {

	}

	public Todo saveTodo(Todo todo) {

		try {
			entityManager.getTransaction().begin();
			todo = entityManager.merge(todo);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}

		return todo;
	}

	public Todo readTodo(long id) {

		Todo todo = entityManager.find(Todo.class, id);
		return todo;
	}

	@SuppressWarnings("unchecked")
	public List<Todo> readAllTodos() {
		List<Todo> todoList = null;

		todoList = entityManager.createQuery("from Todo").getResultList();

		return todoList;
	}

	public void deleteAllTodos() {

		try {
			entityManager.getTransaction().begin();
			entityManager.createQuery("delete from Todo").executeUpdate();
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}

	}

	public void deleteTodo(long id) {
		Todo todo = entityManager.find(Todo.class, id);
		entityManager.remove(todo);
		
	}
}
