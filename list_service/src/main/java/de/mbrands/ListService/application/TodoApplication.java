package de.mbrands.ListService.application;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("resources")
public class TodoApplication extends ResourceConfig {
    public TodoApplication() {
        packages("de.mbrands.ListService.resources");
    }
}
